import pyodbc
# -*- coding: utf-8 -*-

# Arcpy module 
import arcpy

## arcgis module
from arcgis.gis import GIS, User
from arcgis.env import active_gis
##from arcgis.features import FeatureLayerCollection
from arcgis.mapping import WebMap

#import sys
import pandas as pd
import os
import datetime
#from pathlib import Path

class Toolbox(object):
    def __init__(self):
        """Define the toolbox (the name of the toolbox is the name of the
        .pyt file)."""
        self.label = "zHooks"
        self.alias = "zHooks"

        # List of tool classes associated with this toolbox
        self.tools = [CreateEngViews, GrantAccess, CreateHooks, RegisterHooks, SyncHooks, TerminateHooks]
        
class TerminateHooks(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Terminate Data Hooks"
        self.description = ""
        self.canRunInBackground = False
        self.category = 'Data Hooks'
        #self.gis = gis
        #self.gis = GIS("pro")
        #self.me = self.gis.users.me.get('username')   
        #self.folders = self.myUser.folders
        self.dbhost = 'tald011'        
        self.dbdriver = 'SQL Server'        
        self.dbNames = self.get_db_names()

    def getParameterInfo(self):
        """Define parameter definitions"""                
        dbNames = arcpy.Parameter(
            displayName="Databases on TALD011",
            name="dbNames",
            datatype="GPString",
            parameterType="Required",
            enabled = True,
            direction="Input")
        dbNames.filter.type = "ValueList"
        dbNames.filter.list = self.dbNames  
        
        # remove = arcpy.Parameter(
            # displayName = 'Remove Existing Tables',
            # name = 'backup',
            # datatype = 'GPBoolean',
            # parameterType = 'Optional',
            # direction = 'Input')    
        # remove.value = False
        
        items = arcpy.Parameter(
            displayName = 'Items',
            name = 'items',
            datatype = 'GPValueTable',
            parameterType = 'Required',
            direction = 'Input')
        #items.parameterDpendencies = [folders.name]
        
        items.columns = [['GPString','Hook Primary Key'],['GPString', 'Source Url'],['GPString','Target Table'],['GPString','Item Type']]
        params = [dbNames, items]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        if parameters[0].valueAsText:
            if parameters[0].altered:
                dbName = parameters[0].valueAsText
                connstring = 'DRIVER={{{0}}};SERVER={1};DATABASE={2};Trusted_Connection=yes;'.format(self.dbdriver, self.dbhost, dbName)
                conn = pyodbc.connect(connstring)
                cursor = conn.cursor()
                sql = """SELECT PrimaryKeyId, ServiceUrl, TableName, ItemType FROM zHooks.DataHooks;"""    
                cursor.execute(sql)
                results = cursor.fetchall()
                cursor.close()
                conn.close()
                items = []
                for row in results:
                    newRow = [] 
                    for i in row:
                        newRow.append(i)
                    items.append(newRow)
                parameters[2].values = items               
                
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""        
        dbName = parameters[0].valueAsText
        #remove = parameters[1].value
        hooks = parameters[1].value
        connstring = 'DRIVER={{{0}}};SERVER={1};DATABASE={2};Trusted_Connection=yes;'.format(self.dbdriver, self.dbhost, dbName)
        conn = pyodbc.connect(connstring)        
        cursor = conn.cursor()
        for key, url, name, itemType in hooks:
            if remove:
                sql = f'DROP TABLE dbo.{name};'
                cursor.execute(sql)
            sql = f'DELETE FROM zHooks.DataHooks WHERE PrimaryKeyId = {key}'
            cursor.execute(sql)
        conn.commit()
        cursor.close()
        conn.close()        
        return        
    def get_db_names(self):
        connstring = 'DRIVER={{{0}}};SERVER={1};Trusted_Connection=yes;'.format(self.dbdriver, self.dbhost)
        conn = pyodbc.connect(connstring)
        cursor = conn.cursor()
        query = '''SELECT name FROM sys.databases;'''
        cursor.execute(query)
        results = [item[0] for item in cursor.fetchall()]
        cursor.close()
        conn.close()
        results.sort()
        return(results)
        
class SyncHooks(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Sync Data Hooks"
        self.description = ""
        self.canRunInBackground = False
        self.category = 'Data Hooks'
        #self.gis = gis
        #self.gis = GIS("pro")
        #self.me = self.gis.users.me.get('username')   
        #self.folders = self.myUser.folders
        self.dbhost = 'tald011'        
        self.dbdriver = 'SQL Server'        
        self.dbNames = self.get_db_names()

    def getParameterInfo(self):
        """Define parameter definitions"""                
        dbNames = arcpy.Parameter(
            displayName="Databases on TALD011",
            name="dbNames",
            datatype="GPString",
            parameterType="Required",
            enabled = True,
            direction="Input")
        dbNames.filter.type = "ValueList"
        dbNames.filter.list = self.dbNames  
        
        backup = arcpy.Parameter(
            displayName = 'Backup Existing Items',
            name = 'backup',
            datatype = 'GPBoolean',
            parameterType = 'Optional',
            direction = 'Input')    
        backup.value = True
        
        items = arcpy.Parameter(
            displayName = 'Items',
            name = 'items',
            datatype = 'GPValueTable',
            parameterType = 'Required',
            direction = 'Input')
        #items.parameterDpendencies = [folders.name]
        
        items.columns = [['GPString','Hook Primary Key'],['GPString', 'Source Url'],['GPString','Target Table'],['GPString','Item Type']]
        params = [dbNames, backup, items]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        if parameters[0].valueAsText:
            if parameters[0].altered and not parameters[0].hasBeenValidated:
                dbName = parameters[0].valueAsText
                connstring = 'DRIVER={{{0}}};SERVER={1};DATABASE={2};Trusted_Connection=yes;'.format(self.dbdriver, self.dbhost, dbName)
                conn = pyodbc.connect(connstring)
                cursor = conn.cursor()
                sql = """SELECT PrimaryKeyId, ServiceUrl, TableName, ItemType FROM zHooks.DataHooks;"""    
                cursor.execute(sql)
                results = cursor.fetchall()
                cursor.close()
                conn.close()
                items = []
                for row in results:
                    newRow = [] 
                    for i in row:
                        newRow.append(i)
                    items.append(newRow)
                parameters[2].values = items               
                
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""        
        dbName = parameters[0].valueAsText
        backup = parameters[1].value
        hooks = parameters[2].value
        connstring = 'DRIVER={{{0}}};SERVER={1};DATABASE={2};Trusted_Connection=yes;'.format(self.dbdriver, self.dbhost, dbName)
        conn = pyodbc.connect(connstring)        
        cursor = conn.cursor()
        
        # get existing tables in dbo schema
        existingTables = [item[0] for item in cursor.execute("SELECT t.name as table_name, s.name as schema_name FROM sys.tables t INNER JOIN sys.schemas s ON (t.schema_id = s.schema_id) WHERE s.name = 'dbo';").fetchall()]
        
        # folder for an SDE file for DB connection
        scriptPath = os.getcwd()        
        sdeFolder = os.path.join(scriptPath, 'sdeFiles')
        sdeFile = f'{dbName}.sde'
        sdePath = os.path.join(sdeFolder, sdeFile)
        if not os.path.exists(sdeFolder):
            arcpy.AddMessage(f'Creating {sdeFolder}')
            os.mkdir(sdeFolder)
        if not os.path.exists(sdePath):            
            arcpy.AddMessage(f'Creating {sdePath}')
            arcpy.CreateDatabaseConnection_management(sdeFolder, sdeFile, 'SQL_SERVER', 'tald011',
                                                      'OPERATING_SYSTEM_AUTH', database = dbName)
        ## Here we go
        arcpy.env.workspace = sdePath
        now =  datetime.datetime.now().strftime('%Y%m%d_%H%M%S')
        for key, url, table, itemType in hooks:
            tupper = table.upper()
            if table in existingTables:  
                if backup:
                    newName = f"{table}_{now}"                
                    arcpy.AddMessage(f'Backing up {table} to {newName}')
                    sql = """EXEC sp_rename '{0}','{1}';""".format(table, newName)
                    cursor.execute(sql)
                    conn.commit()
                    sql = f'ALTER SCHEMA zzBackup TRANSFER {newName};'
                    cursor.execute(sql)
                    conn.commit()                                
            arcpy.AddMessage(f'Copying {url} to {tupper}') 
            copyStart = datetime.datetime.now()
            if itemType == 'Feature Layer':
                arcpy.FeatureClassToFeatureClass_conversion(url, sdePath, table)
            elif itemType == 'Table Layer':
                arcpy.conversion.TableToTable(url, sdePath, table)
            copyEnd = datetime.datetime.now()
            arcpy.AddMessage(f'Renaming {tupper} to {table}')      
            sql = f"EXEC sp_rename '{tupper}','{table}';"
            cursor.execute(sql)
            conn.commit()
            arcpy.AddMessage('Granting Select Permissions to TALD011\GISSQL_USERS')   
            sql = f'GRANT SELECT ON OBJECT::dbo.{table} TO [TALD011\\GISSQL_Users]'
            cursor.execute(sql)
            conn.commit()
            arcpy.AddMessage(f'Updating zHooks.DataHooks')
            copyTime = ((copyEnd - copyStart).total_seconds()) / 60.0
            sql = f'UPDATE zHooks.DataHooks SET LastSync = GETDATE(), CopyTimeMinutes = {copyTime} WHERE PrimaryKeyId = {key};'
            cursor.execute(sql)
            conn.commit()        
        cursor.close()
        conn.commit()
        conn.close()
        return        
    def get_db_names(self):
        connstring = 'DRIVER={{{0}}};SERVER={1};Trusted_Connection=yes;'.format(self.dbdriver, self.dbhost)
        conn = pyodbc.connect(connstring)
        cursor = conn.cursor()
        query = '''SELECT name FROM sys.databases;'''
        cursor.execute(query)
        results = [item[0] for item in cursor.fetchall()]
        cursor.close()
        conn.close()
        results.sort()
        return(results)
        
class CreateHooks(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Create Hooks Schema/Table"
        self.description = ""
        self.canRunInBackground = False
        self.category = 'Data Hooks'
        #self.gis = gis
        #self.gis = GIS("pro")
        #self.me = self.gis.users.me.get('username')   
        #self.folders = self.myUser.folders
        self.dbhost = 'tald011'        
        self.dbdriver = 'SQL Server'        
        self.dbNames = self.get_db_names()

    def getParameterInfo(self):
        """Define parameter definitions"""                
        dbNames = arcpy.Parameter(
            displayName="Databases on TALD011",
            name="dbNames",
            datatype="GPString",
            parameterType="Required",
            enabled = True,
            direction="Input")
        dbNames.filter.type = "ValueList"
        dbNames.filter.list = self.dbNames
        params = [dbNames]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        dbName = parameters[0].valueAsText
        connstring = 'DRIVER={{{0}}};SERVER={1};DATABASE={2};Trusted_Connection=yes;'.format(self.dbdriver, self.dbhost, dbName)
        conn = pyodbc.connect(connstring)
        cursor = conn.cursor()
        sql = """SELECT * FROM sys.schemas WHERE name = 'zHooks';"""
        cursor.execute(sql)
        results = [item[0] for item in cursor.fetchall()]
        if len(results) == 0:
            arcpy.AddMessage('zHooks not found. Creating zHooks Schema.')
            cursor.execute('CREATE SCHEMA zHooks;')
            conn.commit()
        else:
            arcpy.AddMessage('zHooks schema already exists, skipping.')
        arcpy.AddMessage('Creating/Replacing Table zHooks.DataHooks')
        self.create_hook_tables(cursor)
        conn.commit()
        
        sql = """SELECT * FROM sys.schemas WHERE name = 'ago';"""
        cursor.execute(sql)
        results = [item[0] for item in cursor.fetchall()]
        if len(results) == 0:
            arcpy.AddMessage('ago Schema not found. Creating ago Schema.')
            cursor.execute('CREATE SCHEMA ago;')
            conn.commit()
        else:
            arcpy.AddMessage('ago schema already exists, skipping.')
        sql = """SELECT * FROM sys.schemas WHERE name = 'zzBackup';"""
        cursor.execute(sql)
        results = [item[0] for item in cursor.fetchall()]
        if len(results) == 0:
            arcpy.AddMessage('zzBackup Schema not found. Creating zzBackup Schema.')
            cursor.execute('CREATE SCHEMA zzBackup;')
            conn.commit()
        else:
            arcpy.AddMessage('zzBackup schema already exists, skipping.')          
        conn.commit()
        cursor.close()
        conn.close()        
        return
        
    def get_db_names(self):
        connstring = 'DRIVER={{{0}}};SERVER={1};Trusted_Connection=yes;'.format(self.dbdriver, self.dbhost)
        conn = pyodbc.connect(connstring)
        cursor = conn.cursor()
        query = '''SELECT name FROM sys.databases;'''
        cursor.execute(query)
        results = [item[0] for item in cursor.fetchall()]
        cursor.close()
        conn.close()
        results.sort()
        return(results)
    def create_hook_tables(self, cursor):
        sql = '''
                DROP TABLE IF EXISTS zHooks.DataHooks;
                CREATE TABLE zHooks.DataHooks (
                    PrimaryKeyId INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
                    ItemId VARCHAR(255),
                    ServiceUrl VARCHAR(1000),
                    TableName VARCHAR(255),                    
                    ItemType VARCHAR(50),
                    LastSync DATETIME,
                    CopyTimeMinutes FLOAT,                    
                    HookRegistered DATETIME DEFAULT CURRENT_TIMESTAMP
                    );'''
        cursor.execute(sql)
        return
                    

        
        
class RegisterHooks(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Register Data Hooks"
        self.description = "Register Hooks to Database"
        self.category = 'Data Hooks'
        self.canRunInBackground = False
        
        self.gis = GIS("pro")
        self.me = self.gis.users.me.get('username')
        self.myUser = User(self.gis, self.me)
        self.folders = self.myUser.folders       
        self.dbhost = 'tald011'        
        self.dbdriver = 'SQL Server'  
        self.dbNames = self.get_db_names()
        # Default Folders are folders of pro user
        self.folders = [item.get('title') for item in self.folders] 
        # Default Access information  

    def getParameterInfo(self):
        """Define parameter definitions"""
        """Define parameter definitions"""                
        dbNames = arcpy.Parameter(
            displayName="Databases on TALD011",
            name="dbNames",
            datatype="GPString",
            parameterType="Required",
            enabled = True,
            direction="Input")
        dbNames.filter.type = "ValueList"
        dbNames.filter.list = self.dbNames
        folders = arcpy.Parameter(
            displayName="AGO/Portal Folders",
            name="folders",
            datatype="GPString",
            parameterType="Optional",
            enabled = True,
            direction="Input")
        folders.filter.type = "ValueList"
        folders.filter.list = self.folders
        items = arcpy.Parameter(
            displayName = 'Items',
            name = 'items',
            datatype = 'GPValueTable',
            parameterType = 'Required',
            direction = 'Input')
        #items.parameterDpendencies = [folders.name]
        
        items.columns = [['GPString', 'Item Name'], ['GPString', 'Item ID'], ['GPString','URL'], ['GPString', 'Database Table'],['GPString','Item Type']]
        params = [dbNames, folders, items]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        if parameters[1].valueAsText:
            if parameters[1].altered and not parameters[1].hasBeenValidated:
                f = parameters[1].valueAsText
                items = [item for item in self.myUser.items(folder = f) if item.type == 'Feature Service' ]
                updateItems = []
                for item in items:
                    for layer in item.layers:
                        updateItems.append([item.name, item.id, layer.url, layer.properties.get('name'),'Feature Layer'])              
                    for table in item.tables:
                        updateItems.append([item.name, item.id, table.url, item.name, 'Table Layer'])
                parameters[2].values = updateItems
        else:
            parameters[2].values = None
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        dbName = parameters[0].valueAsText
        folder = parameters[1].valueAsText
        items = parameters[2].value
        connstring = 'DRIVER={{{0}}};SERVER={1};DATABASE={2};Trusted_Connection=yes;'.format(self.dbdriver, self.dbhost, dbName)
        conn = pyodbc.connect(connstring)
        cursor = conn.cursor()
        sql = '''INSERT INTO zHooks.DataHooks (ItemId, ServiceUrl, TableName, ItemType) VALUES (?, ?, ?,?)'''
        for name, id, url, tableName, itemType in items:
            cursor.execute(sql, (id, url, tableName, itemType))
        conn.commit()
        cursor.close()
        conn.close()
        return      
        
    def get_db_names(self):
        connstring = 'DRIVER={{{0}}};SERVER={1};Trusted_Connection=yes;'.format(self.dbdriver, self.dbhost)
        conn = pyodbc.connect(connstring)
        cursor = conn.cursor()
        query = '''SELECT name FROM sys.databases;'''
        cursor.execute(query)
        results = [item[0] for item in cursor.fetchall()]
        cursor.close()
        conn.close()
        results.sort()
        return(results)


       
class GrantAccess(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Grant Access to GISSQL_Users"
        self.description = ""
        self.canRunInBackground = False
        self.category = 'Access'
        #self.gis = gis
        #self.gis = GIS("pro")
        #self.me = self.gis.users.me.get('username')   
        #self.folders = self.myUser.folders
        self.dbhost = 'tald011'        
        self.dbdriver = 'SQL Server'        
        self.dbNames = self.get_db_names()

    def getParameterInfo(self):
        """Define parameter definitions"""                
        dbNames = arcpy.Parameter(
            displayName="Databases on TALD011",
            name="dbNames",
            datatype="GPString",
            parameterType="Required",
            enabled = True,
            direction="Input")
        dbNames.filter.type = "ValueList"
        dbNames.filter.list = self.dbNames
        user = arcpy.Parameter(
            displayName="Grant Access to User",
            name="user",
            datatype="GPString",
            parameterType="Required",
            enabled = True,
            direction="Input")
        user.value = r'TALD011\GISSQL_Users'
        
        schema = arcpy.Parameter(
            displayName="Grant Access On Schema",
            name="schema",
            datatype="GPString",
            parameterType="Required",
            enabled = True,
            direction="Input")
        schema.value = 'dbo'
        
        params = [dbNames, user, schema]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        dbName = parameters[0].valueAsText
        userName= parameters[1].valueAsText
        schemaName= parameters[2].valueAsText
        connstring = 'DRIVER={{{0}}};SERVER={1};DATABASE={2};Trusted_Connection=yes;'.format(self.dbdriver, self.dbhost, dbName)
        conn = pyodbc.connect(connstring)
        cursor = conn.cursor()
        try:
            sql = """select * from sys.database_principals WHERE NAME = '{0}';""".format(userName)
            users = [item for item in cursor.execute(sql).fetchall()]
            if len(users) == 0:
                sql = """CREATE USER [{0}] FOR LOGIN [{0}]""".format(userName)
                cursor.execute(sql)
                conn.commit()
            sql = """EXEC sp_addrolemember N'db_datareader', N'{0}'""".format(userName)
            cursor.execute(sql)
            conn.commit()
            sql = """GRANT SELECT ON SCHEMA::{0} TO [{1}];""".format(schemaName, userName)
            cursor.execute(sql)
            conn.commit()
            sql = """GRANT SELECT ON SCHEMA::{0} to [{1}];""".format('zHooks', userName)
            cursor.close()
            conn.close()
        except Exception as e:
            arcpy.AddMessage(str(e))
            arcpy.AddMessage(sql)
            cursor.close()
            conn.close()
        return
        
    def get_db_names(self):        
        connstring = 'DRIVER={{{0}}};SERVER={1};Trusted_Connection=yes;'.format(self.dbdriver, self.dbhost)
        conn = pyodbc.connect(connstring)
        cursor = conn.cursor()
        query = '''SELECT name FROM sys.databases;'''
        cursor.execute(query)
        results = [item[0] for item in cursor.fetchall()]
        cursor.close()
        conn.close()
        results.sort()
        return(results)
    def create_hook_tables(self, cursor):
        sql = '''
                DROP TABLE IF EXISTS zHooks.DataHooks;
                CREATE TABLE zHooks.DataHooks (
                    PrimaryKeyId INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
                    ItemId VARCHAR(255),
                    ServiceUrl VARCHAR(1000),
                    TableName VARCHAR(255),                    
                    ItemType VARCHAR(50),
                    LastSync DATETIME,
                    CopyTimeMinutes FLOAT,                    
                    HookRegistered DATETIME DEFAULT CURRENT_TIMESTAMP
                    );'''
        cursor.execute(sql)
        return
        
class CreateEngViews(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Create Engineering Views"
        self.description = ""
        self.canRunInBackground = False
        self.category = 'Access'
        self.dbhost = 'tald011'        
        self.dbdriver = 'SQL Server'        
        self.dbNames = self.get_db_names()

    def getParameterInfo(self):
        """Define parameter definitions"""                
        dbNames = arcpy.Parameter(
            displayName="Databases on TALD011",
            name="dbNames",
            datatype="GPString",
            parameterType="Required",
            enabled = True,
            direction="Input")
        dbNames.filter.type = "ValueList"
        dbNames.filter.list = self.dbNames

        params = [dbNames]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""  
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return
        
    def execute(self, parameters, messages):
        """The source code of the tool."""
        dbName = parameters[0].valueAsText
        connstring = 'DRIVER={{{0}}};SERVER={1};DATABASE={2};Trusted_Connection=yes;'.format(self.dbdriver, self.dbhost, dbName)
        conn = pyodbc.connect(connstring)
        cursor = conn.cursor()
        try:
            if self.create_schema(cursor):
                conn.commit()
            tables = self.list_tables(cursor)
            for table in tables:
                columns = self.list_columns(cursor, table)
                self.build_view(conn, cursor, table, columns)
                conn.commit()
            sql = """GRANT SELECT ON SCHEMA::engineer TO [{1}];""".format('tald011\GISSQL_Users')
            cursor.execute(sql)
            conn.commit()                
        except Exception as e:
            arcpy.AddMessage(str(e))            
        finally:
            cursor.close()
            conn.close()
        return  
        
    def get_db_names(self):        
        connstring = 'DRIVER={{{0}}};SERVER={1};Trusted_Connection=yes;'.format(self.dbdriver, self.dbhost)
        conn = pyodbc.connect(connstring)
        cursor = conn.cursor()
        query = '''SELECT name FROM sys.databases;'''
        cursor.execute(query)
        results = [item[0] for item in cursor.fetchall()]
        cursor.close()
        conn.close()
        results.sort()
        return(results)
        
    def list_columns(self, cursor, table):
        '''Get a list of columns for a table in the database'''
        sql = f"SELECT name FROM sys.columns WHERE object_id = OBJECT_ID('{table}') and system_type_id != 240;"
        result = [item[0] for item in cursor.execute(sql).fetchall()]
        return(result)
        
    def create_schema(self, cursor):
        sql = """SELECT * FROM sys.schemas WHERE name = 'engineer';"""        
        results = [item[0] for item in cursor.execute(sql).fetchall()]
        if len(results) == 0:
            arcpy.AddMessage('engineer not found. Creating engineer Schema.')
            cursor.execute('CREATE SCHEMA engineer;')
            return(True)
        else:
            arcpy.AddMessage('engineer schema already exists, skipping.')
            return(False)
            
    def list_tables(self, cursor):
        sql = """SELECT TableName FROM zHooks.DataHooks;"""
        result = [item[0] for item in cursor.execute(sql).fetchall()]
        return(result)
        
    def build_view(self, conn, cursor, table, columns):
        exists = f"SELECT t.name FROM sys.views t INNER JOIN sys.schemas s ON (t.schema_id = s.schema_id) WHERE s.name = 'engineer' and t.name = '{table}'"        
        existing = [item[0] for item in cursor.execute(exists).fetchall()]
        columnSelection = ", ".join(columns)
        if len(existing) == 1:
            arcpy.AddMessage(f'Dropping view engineer.{table}')
            cursor.execute(f"DROP VIEW engineer.{table};")
            conn.commit()
        sql = f"CREATE VIEW engineer.{table} AS (SELECT {columnSelection} FROM dbo.{table});"
        cursor.execute(sql)
        arcpy.AddMessage(f'Creating view engineer.{table}')
        return
 