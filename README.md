# README #

HNTB AGOL and Portal Utility Toolboxes. **These tools require ArcPro 2.9.3 and Python 3.7.11. They are not compatible with ArcMap/ArcCatalog/ArcGIS Desktop.**  

### Current Tools ###

* AGOL Utilities  
    + Authentication  
        + Get Token  
    + Metadata  
        + Apply Basic Details to Folder  
        + Apply Item Description to Folder  
        + Apply Summary to Folder  
        + Apply Tags to Folder  
        + Item Inventory to Excel  
    + Migration  
        + Clone Items Across Portals (In Development)  

### Local Setup ###

First, clone this repository to your local machine in one of two ways:   
  
Using git-bash: `git clone https://sethbassett@bitbucket.org/sethbassett/hntb-agol-and-portal-utilities.git`  
  
Using Bitbucket web interface:  
  
    1. Navigate to the [repository webpage](https://bitbucket.org/sethbassett/hntb-agol-and-portal-utilities/src/master/)  
    2. Click the "..." button
    3. Select "Download Repository."  
    4. Unzip the downloaded repository to a stable folder/directory on your local machine.  
    
Next, add the toolboxes to an ArcPro project:  
  
    1. Open a catalog pane or catalog view.  
    2. Under the "Project" tree, right click on "Toolboxes" and select "Add Toolbox."  
    3. Using the dialog, add the python toolbox (.pyt) file to your project.  
      
ArcPro might take 60-90 seconds to read the python toolbox file and validate the underlying code.   

### Developer Contact ###

* [Seth Bassett, Geospatial Systems Developer II](mailto:sbassett@hntb.com)  
